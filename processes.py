import array
import sys
import config


def ampli(sound, factor: float):
    nsamples = config.samples_second * int(sys.argv[4])
    sounds = array.array('h', [0] * nsamples)
    for i in range(nsamples):
        if sounds[i] > config.max_amp:
            sounds[i] = config.max_amp
        elif sounds[i] < -config.min_amp:
            sounds[i] = -config.min_amp
        else:
            sounds[i] *= factor
    return sound



def reduce(sound, factor: int):
    nsamples = config.samples_second * int(sys.argv[4])
    sounds = array.array('h', [0] * nsamples)

    if factor > 0:
        del sounds[::factor]
        return sound
    else:
        print("A de ser mayor a 0")




def extend(sound, factor: int):
    nsamples = config.samples_second * int(sys.argv[4])
    numero = 0

    if factor > 0:
        for i in range(factor, nsamples, factor):
            if i > factor:
                i *= numero
                sound.insert(i, (sound[i - 1]) + sound[1] // 2)
            else:
                sound.insert(i, (sound[i - 1]) + sound[1] // 2)
        return sound
    else:
        print("A de ser mayor a 0")
